<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>つぶやき編集画面</title>
</head>
<body>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
	</c:if>
	<form action="edit" method="post">
		<input type="hidden" name="messageId" value="${message.id}">
		つぶやき<br />
		<textarea name="text" cols="100" rows="5"><c:out value="${message.text}" /></textarea><br />
		<input type="submit" value="更新"> <br>
	</form>
		<a href="./">戻る</a>


	<div class="copyright">Copyright(c)TomokiTakeshita</div>
</body>
</html>